import React from "react";
import "./contact.css";
import { HiOutlineMail } from "react-icons/hi";
import { PiMessengerLogo } from "react-icons/pi";
import { BiLogoWhatsapp } from "react-icons/bi";
const Contact = () => {
  return (
    <section id="contact">
      <h5>Get In Toch</h5>
      <h2>Contact Me</h2>

      <div className="container contact__container">
        <div className="contact__options">
          <article className="contact__option">
            <HiOutlineMail className="contact_option-icon" />
            <h4>Email</h4>
            <h5>amir.mirdamad110@gmail.com</h5>
            <a href="mailto:amir.mirdamad110@gmail.com">Send a message</a>
          </article>
          <article className="contact__option">
            <PiMessengerLogo className="contact__option-icon" />
            <h4>Messenger</h4>
            <h5>Mirdamadi@amir</h5>
            <a href="mailto:amir.mirdamad110@gmail.com">Send a message</a>
          </article>
          <article className="contact__option">
            <BiLogoWhatsapp className="contact_option-icon" />
            <h4>WhatsApp</h4>
            <h5>+21389229</h5>
            <a href="mailto:amir.mirdamad110@gmail.com">Send a message</a>
          </article>
        </div>
        {/* End Of Contact Option */}
        <form action="">
          <input
            type="text"
            name="name"
            placeholder="Your Full Name"
            required
          />
          <input type="Email" name="email" placeholder="Your Email" required />
          <textarea
            name="message"
            rows="7"
            placeholder="Your Message"
            required></textarea>

          <button type="submit" className="btn btn-primary">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
