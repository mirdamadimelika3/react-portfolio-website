import React from "react";
import "./experience.css";
import { BsPatchCheckFill } from "react-icons/bs";
const Experience = () => {
  return (
    <section id="experience">
      <h5>What Skills I have</h5>
      <h2>My Experience</h2>

      <div className="container experience__container">
        <div className="experince__frontend">
          <h3>Designer and Analyzing</h3>
          <div className="experince__content">
            <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>SolidWork</h4>
                <small className="text-light">Experienced</small>
              </div>
            </article>
            <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>PLC</h4>
                <small className="text-light">Experienced</small>
              </div>
            </article>
            <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>ABAQUS</h4>
                <small className="text-light">Experienced</small>
              </div>
            </article>
            <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>CATIA</h4>
                <small className="text-light">Experienced</small>
              </div>
            </article>
            {/* <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>React</h4>
                <small className="text-light">Intermediate</small>
              </div>
            </article>
            <article className="experince__details">
              <BsPatchCheckFill className="experience__details-icon" />
              <div>
                <h4>Git</h4>
                <small className="text-light">Experienced</small>
              </div>
            </article> */}
            {/* <article className="experince__details">
                 <BsPatchCheckFill/>
                 <h4>Python</h4>
                 <small className="text-light">Experienced</small>
              </article> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Experience;
