import React from "react";
import "./portfolio.css";
import p1 from "../../assets/p1.jpg";
import p2 from "../../assets/p2.jpg";
import p3 from "../../assets/p3.jpg";

const Portfolio = () => {
  return (
    <section id="portfolio">
      <h5>My Project</h5>
      <h2>Portfolio</h2>

      <div className="containre portfolio__container">
        <article className="portfolio__item">
          <div className="portfolio__item-img">
            <img src={p1} alt="" />
            <h3>Sungear Analysis</h3>
          </div>
        </article>
        <article className="portfolio__item">
          <div className="portfolio__item-img">
            <img src={p2} alt="" />
            <h3>Gearbox Analysis</h3>
          </div>
        </article>
        <article className="portfolio__item">
          <div className="portfolio__item-img">
            <img src={p3} alt="" />
            <h3>Helical Gearbox Analysis</h3>
          </div>
        </article>
      </div>
    </section>
  );
};

export default Portfolio;
