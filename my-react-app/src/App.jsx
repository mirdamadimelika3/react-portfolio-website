import React from "react";
import {
  Header,
  Contact,
  Experience,
  Footer,
  Nav,
  Portfolio,
  About,
} from "./components";

const App = () => {
  return (
    <>
      <Header />
      <Nav />
      <About />
      <Experience />
      <Portfolio />
      <Contact />
      <Footer />
    </>
  );
};

export default App;
